package cloud.honeydue.api.controller;

import cloud.honeydue.api.model.Chore;
import cloud.honeydue.api.model.User;
import cloud.honeydue.api.service.ChoreService;
import cloud.honeydue.api.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Chore Controller", description = "Chore API")
@RequestMapping(value="/api/v1", produces="application/json")
@CrossOrigin(origins="*")
class ChoreController {
    Logger logger = LoggerFactory.getLogger(ChoreController.class);
    private final ChoreService choreService;
    private final UserService userService;

    ChoreController(ChoreService choreService, UserService userService) {
        this.choreService = choreService;
        this.userService = userService;
    }

    @GetMapping("/chores/{nickname}")
    public List<Chore> findChoresForNickname(@PathVariable("nickname") String nickname) {
        Optional<User> user = userService.getByNickname(nickname);
        if (user.isPresent()) {
            logger.info(user.get().getNickname());
            return choreService.findByUserId(user.get().getId());
        } else {
            logger.info("User not found.");
            return null;
        }
    }

}
